# Init

## Installation

To setup the system, run this command:

```
/bin/sh -c "$(curl https://gitlab.com/api/v4/projects/19490471/repository/files/init/raw\?ref\=master)"
```

To run the lite version, run this command:

```
/bin/sh -c "$(curl https://gitlab.com/api/v4/projects/19490471/repository/files/init-lite/raw\?ref\=master)"
```

## Other

Add a Chrome Debug shortcut:

- Open Automator
- Choose Application when it prompts you for what kind of automation you want to make
- Find and choose Run Shell Script from the list of actions
- Put your shell command to launch Chrome with the flag in the field: /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --remote-debugging-port=9222
- Save your automation somewhere with a convenient name you can reference (e.g. Google Chrome Debug Mode) I saved mine to ~/Applications so it would appear in my Applications list.
